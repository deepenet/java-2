package Calculator;

import Exceptions.*;
import Factory.Factory;
import Operation.Operation;


import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    // classes
    static public class ExecutionContext {
        // fields
        Vector<Double> stack;
        Map<String, Double> definitions;

        // methods
        public ExecutionContext() {
            stack = new Vector<>();
            definitions = new HashMap<>();
        }
        public void push(Double value) {
            stack.add(value);
        }
        public void pop() throws StackEmptyException {
            if(stack.isEmpty()) {
                throw new StackEmptyException();
            }
            stack.remove(stack.size() - 1);
        }
        public Double showBack() throws StackEmptyException {
            if(stack.isEmpty()) {
                throw new StackEmptyException();
            }
            return stack.get(stack.size() - 1);
        }
        public boolean isEmpty() {
            return stack.isEmpty();
        }
        public Integer size() {
            return stack.size();
        }
        public void define(String name, Double value) {
            definitions.put(name, value);
        }
        public Double definitionValue(String name) {
            return definitions.get(name);
        }
    }

    // fields
    private ExecutionContext executionContext = new ExecutionContext();

    // methods
    public void run(BufferedReader streamReader) throws IOException {
        // create factory
        Factory factory = new Factory();
        // do work until end of file will come or until exception will be generated
        while(true) {
            try {
                // preparing
                String lineWithOperation;   // contains line with operation
                String operationName;       // contains operation name
                List<String> parametersList = new ArrayList<>();    // contains list of parameters
                Operation operation;        // object of functor - use method doOperation for action
                // read command
                lineWithOperation = streamReader.readLine();
                if(lineWithOperation == null) {
                    break;
                }
                // parse command
                Pattern pattern = Pattern.compile("[^ \t]+",
                        Pattern.UNICODE_CHARACTER_CLASS | Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(lineWithOperation);
                if (!matcher.find()) {
                    throw new CommandNotFoundException();
                }
                operationName = matcher.group();
                while (matcher.find()) {
                    parametersList.add(matcher.group());
                }
                // create needed operation
                operation = factory.createOperation(operationName);
                // execute the operation with parameters list
                operation.perform(executionContext, parametersList);
            } catch (IOException | IllegalAccessException | InstantiationException | ClassNotFoundException e) {
                e.printStackTrace();
            } catch (StackEmptyException | BadOperationParametersException | NotEnoughStackCapacityException | CommandNotFoundException | DoubleDefineException e) {
                System.out.println("ERROR: " + e.getMessage());
            }
        }
    }
}