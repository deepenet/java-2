package Exceptions;

public class BadOperationParametersException extends Exception {
    @Override
    public String getMessage() {
        return "The number of parameters is not enough";
    }
}