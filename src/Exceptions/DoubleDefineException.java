package Exceptions;

public class DoubleDefineException extends Exception {
    @Override
    public String getMessage() {
        return "The number can not be defined";
    }
}
