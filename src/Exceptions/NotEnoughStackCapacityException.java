package Exceptions;

public class NotEnoughStackCapacityException extends Exception {
    @Override
    public String getMessage() {
        return "The number of items in the stack is not correct";
    }
}
