package Factory;

import Exceptions.CommandNotFoundException;
import Operation.*;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;


public class Factory {
    // fields
    private Properties listNamesOfOperations = new Properties();

    // methods
    //  constructors
    public Factory() throws IOException {
        // load information from configuration file
        File file = new File("src/Factory/configFactory.properties");
        listNamesOfOperations.load(new FileReader(file));
    }
    //  create operation
    public Operation createOperation(String name) throws IllegalAccessException, InstantiationException, ClassNotFoundException, CommandNotFoundException {
        if(listNamesOfOperations.getProperty(name) == null) {
            throw new CommandNotFoundException();
        }
        Class forInstance = Class.forName(listNamesOfOperations.getProperty(name));
        return (Operation) forInstance.newInstance();
    }
}
