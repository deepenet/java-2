import Calculator.*;

import java.io.*;

public class Main {
    public static void main(String[] argv) throws IOException {
        BufferedReader streamReader;
        if(argv.length >= 1) {
            try {
                streamReader = new BufferedReader(new FileReader(argv[0]));
            } catch (java.io.FileNotFoundException except) {
                System.out.println("File cannot be opened.\nUsing standard input stream\n");
                streamReader = new BufferedReader(new InputStreamReader(System.in));
            }
        } else {
            streamReader = new BufferedReader(new InputStreamReader(System.in));
        }
        try {
            Calculator calc = new Calculator();
            calc.run(streamReader);
        } catch (Exception except){
            System.out.println("Exception " + except.toString() + " was thrown\n");
        }
        streamReader.close();
    }
}
