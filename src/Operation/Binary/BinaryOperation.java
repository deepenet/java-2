package Operation.Binary;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Exceptions.NotEnoughStackCapacityException;
import Exceptions.StackEmptyException;
import Operation.Operation;

import java.util.List;

abstract public class BinaryOperation implements Operation {
    Double first, second;
    @Override
    public void perform(Calculator.ExecutionContext executionContext, List<String> parametersList) throws StackEmptyException, NotEnoughStackCapacityException, BadOperationParametersException  {
        if(executionContext.isEmpty()) {
            throw new StackEmptyException();
        }
        if(executionContext.size() < 2) {
            throw new NotEnoughStackCapacityException();
        }
        if(parametersList.size() > 0) {
            throw new BadOperationParametersException();
        }
        first = executionContext.showBack();
        executionContext.pop();
        second = executionContext.showBack();
        executionContext.pop();
    }
}
