package Operation.Binary;

import Calculator.Calculator;
import Exceptions.*;

import java.util.List;

public class DIV extends BinaryOperation {

    @Override
    public void perform(Calculator.ExecutionContext executionContext, List<String> parametersList) throws StackEmptyException, NotEnoughStackCapacityException, BadOperationParametersException {
        super.perform(executionContext, parametersList);
        executionContext.push(first / second);
    }
}
