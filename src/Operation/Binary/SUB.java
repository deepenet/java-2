package Operation.Binary;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Exceptions.NotEnoughStackCapacityException;
import Exceptions.StackEmptyException;
import java.util.List;

public class SUB extends BinaryOperation {
    @Override
    public void perform(Calculator.ExecutionContext executionContext, List<String> parametersList) throws StackEmptyException, NotEnoughStackCapacityException, BadOperationParametersException {
        super.perform(executionContext, parametersList);
        executionContext.push(first - second);
    }
}