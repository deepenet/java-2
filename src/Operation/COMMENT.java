package Operation;

import Calculator.Calculator;

import java.util.List;

public class COMMENT implements Operation {
    @Override
    public void perform(Calculator.ExecutionContext executionContext, List<String> parametersList) {
        System.out.print("#");
        for(String iter : parametersList) {
            System.out.print(" " + iter);
        }
        System.out.println();
    }
}
