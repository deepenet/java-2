package Operation;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Exceptions.DoubleDefineException;

import java.util.List;

public class DEFINE implements Operation {

    @Override
    public void perform(Calculator.ExecutionContext executionContext, List<String> parametersList) throws DoubleDefineException, BadOperationParametersException {
        if(parametersList.size() != 2) {
            throw new BadOperationParametersException();
        }
        try {
            Double.parseDouble(parametersList.get(0));
            throw new DoubleDefineException();
        } catch (NumberFormatException ignored) {}
        Double value = Double.parseDouble(parametersList.get(1));
        executionContext.define(parametersList.get(0), value);
    }
}