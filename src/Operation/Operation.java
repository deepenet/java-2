package Operation;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Exceptions.DoubleDefineException;
import Exceptions.NotEnoughStackCapacityException;
import Exceptions.StackEmptyException;

import java.util.List;

public interface Operation {
    // methods
    void perform(Calculator.ExecutionContext executionContext, List<String> parametersList) throws StackEmptyException, NotEnoughStackCapacityException, BadOperationParametersException, DoubleDefineException;
}