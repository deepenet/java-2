package Operation;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;

import java.util.List;

public class PUSH implements Operation {
    @Override
    public void perform(Calculator.ExecutionContext executionContext, List<String> parametersList) throws BadOperationParametersException {
        if(parametersList.size() != 1) {
            throw new BadOperationParametersException();
        }
        Double result = executionContext.definitionValue(parametersList.get(0));
        if(result == null) {
            executionContext.push(Double.parseDouble(parametersList.get(0)));
        } else {
            executionContext.push(result);
        }
    }
}
