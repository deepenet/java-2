package Operation.Unary;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Exceptions.StackEmptyException;

import java.util.List;

public class PRINT extends UnaryOperation {
    @Override
    public void perform(Calculator.ExecutionContext executionContext, List<String> parametersList) throws StackEmptyException, BadOperationParametersException {
        super.perform(executionContext, parametersList);
        System.out.println(executionContext.showBack());
    }
}
