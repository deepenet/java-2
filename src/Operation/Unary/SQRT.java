package Operation.Unary;


import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Exceptions.StackEmptyException;

import java.util.List;

import static java.lang.Math.sqrt;

public class SQRT extends UnaryOperation {
    @Override
    public void perform(Calculator.ExecutionContext executionContext, List<String> parametersList) throws StackEmptyException, BadOperationParametersException {
        super.perform(executionContext, parametersList);
        executionContext.pop();
        executionContext.push(sqrt(first));
    }
}
