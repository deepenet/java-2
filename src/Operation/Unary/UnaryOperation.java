package Operation.Unary;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Exceptions.StackEmptyException;
import Operation.Operation;

import java.util.List;

abstract public class UnaryOperation implements Operation {
    Double first;
    @Override
    public void perform(Calculator.ExecutionContext executionContext, List<String> parametersList) throws StackEmptyException, BadOperationParametersException {
        if(executionContext.isEmpty()) {
            throw new StackEmptyException();
        }
        if(parametersList.size() > 0) {
            throw new BadOperationParametersException();
        }
        first = executionContext.showBack();
    }
}