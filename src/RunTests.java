import Tests.*;


public class RunTests {
    public static void main(String[] argv) {
        Calculator_ExecutionContextTest.runTest();
        // Test exceptions in abstract BinaryOperation class based on SUM. Should test correct work of SUM before
        SUMTest.runTest();
        BinaryOperationTest.runTest();
        SUBTest.runTest();
        MULTTest.runTest();
        DIVTest.runTest();
        SQRTTest.runTest();
        UnaryOperationTest.runTest();
        DEFINETest.runTest();
        PUSHTest.runTest();
    }
}