package Tests;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Exceptions.NotEnoughStackCapacityException;
import Exceptions.StackEmptyException;
import Operation.Binary.BinaryOperation;
import Operation.Binary.SUM;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BinaryOperationTest {
    protected static Calculator.ExecutionContext executionContext;
    protected static BinaryOperation operation;
    protected static List<String> list;

    static void actualTest(Double first, Double second, Double expected) throws StackEmptyException, BadOperationParametersException, NotEnoughStackCapacityException {
        executionContext.push(second);
        executionContext.push(first);
        operation.perform(executionContext, list);
        assertEquals(expected, executionContext.showBack());
        executionContext.pop();
    }

    @Test
    public static void runTest() {
        executionContext = new Calculator.ExecutionContext();
        list = new ArrayList<>();
        operation = new SUM();
        try {
            try {
                operation.perform(executionContext, list);
            } catch (StackEmptyException e) {
                assertNotEquals("", e.getMessage());
            }
            executionContext.push(1.1);
            try {
                operation.perform(executionContext, list);
            } catch (NotEnoughStackCapacityException e) {
                assertNotEquals("", e.getMessage());
            }
            List<String> list = new ArrayList<>();
            executionContext.push(2.2);
            operation.perform(executionContext, list);
            executionContext.push(3.3);
            list.add("First_needless");
            try {
                operation.perform(executionContext, list);
            } catch (BadOperationParametersException e) {
                assertNotEquals("", e.getMessage());
            }
            list.add("Second_needless");
            try {
                operation.perform(executionContext, list);
            } catch (BadOperationParametersException e) {
                assertNotEquals("", e.getMessage());
            }
        } catch (Exception e) {
            Assert.fail("Unexpected exception " + e.getMessage());
        }
    }
}