package Tests;

import Calculator.Calculator;
import Exceptions.StackEmptyException;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Calculator_ExecutionContextTest {

    @Test
    public static void runTest() {
        try {
            Calculator.ExecutionContext executionContext = new Calculator.ExecutionContext();
            /*
            *   isEmpty is covered by tests
            *   push is covered by tests
            *   pop is covered by tests
            *   showBack is covered by tests
            *   size is covered by tests
            *   define is covered by tests
            *   definitionValue is covered by tests
            */

            // test isEmpty
            assertTrue(executionContext.isEmpty());

            // test define and definitionValue
            executionContext.define("a", 123.456);
            executionContext.define("b", -123e-4);
            executionContext.define("c", 0.1e15);
            executionContext.define("d", 0.0);
            assertEquals(123.456, executionContext.definitionValue("a"));
            assertEquals(-123e-4, executionContext.definitionValue("b"));
            assertEquals(0.1e15, executionContext.definitionValue("c"));
            assertEquals(0.0, executionContext.definitionValue("d"));
            executionContext.define("a", -654.321);
            assertEquals(-654.321, executionContext.definitionValue("a"));
            assertNotEquals(123.456, executionContext.definitionValue("a"));
            assertNull(executionContext.definitionValue("NotExist"));

            // parallel test of all methods
            executionContext.push(1.0);
            assertFalse(executionContext.isEmpty());
            assertEquals(1, executionContext.size());
            assertEquals(1.0, executionContext.showBack());
            executionContext.push(-123.123);
            assertEquals(2, executionContext.size());
            assertEquals(-123.123, executionContext.showBack());
            executionContext.push(-1e-5);
            assertEquals(3, executionContext.size());
            assertEquals(-1e-5, executionContext.showBack());
            executionContext.push(1e5);
            assertEquals(4, executionContext.size());
            assertEquals(1e5, executionContext.showBack());
            executionContext.push(0.0);
            assertEquals(5, executionContext.size());
            assertEquals(0.0, executionContext.showBack());

            // test pop and size
            executionContext.pop();
            assertEquals(4, executionContext.size());
            executionContext.pop();
            assertEquals(3, executionContext.size());
            executionContext.pop();
            assertEquals(2, executionContext.size());
            executionContext.pop();
            assertEquals(1, executionContext.size());
            executionContext.pop();
            assertEquals(0, executionContext.size());
            assertTrue(executionContext.isEmpty());

            // test pop StackEmptyException
            try {
                executionContext.pop();
                Assert.fail("Expected StackEmptyException");
            } catch (StackEmptyException e) {
                assertNotEquals("", e.getMessage());
            }

            // test showBack StackEmptyException
            try {
                executionContext.showBack();
                Assert.fail("Expected StackEmptyException");
            } catch (StackEmptyException e) {
                assertNotEquals("", e.getMessage());
            }



        } catch (Exception e) {
            Assert.fail("Unexpected exception " + e.toString());
        }
    }
}