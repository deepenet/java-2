package Tests;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Exceptions.DoubleDefineException;
import Operation.*;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DEFINETest {
    @Test
    public static void runTest() {
        Calculator.ExecutionContext executionContext = new Calculator.ExecutionContext();
        Operation operation = new DEFINE();
        List<String> list = new ArrayList<>();
        try{
            try {
                operation.perform(executionContext, list);
                Assert.fail("Expected BadOperationParametersException");
            } catch (BadOperationParametersException e) {
                assertNotEquals("", e.getMessage());
            }
            list.add("1.23");
            try {
                operation.perform(executionContext, list);
                Assert.fail("Expected BadOperationParametersException");
            } catch (BadOperationParametersException e) {
                assertNotEquals("", e.getMessage());
            }
            list.add("567");
            try {
                operation.perform(executionContext, list);
                Assert.fail("Expected DoubleDefineException");
            } catch (DoubleDefineException e) {
                assertNotEquals("", e.getMessage());
            }
            list.add("567");
            try {
                operation.perform(executionContext, list);
                Assert.fail("Expected BadOperationParametersException");
            } catch (BadOperationParametersException e) {
                assertNotEquals("", e.getMessage());
            }
            list.clear();
            list.add("a");
            list.add("1.0");
            operation.perform(executionContext, list);
            assertEquals(1.0, executionContext.definitionValue("a"));
        } catch (Exception e) {
            Assert.fail("Unexpected exception " + e.getMessage());
        }

    }
}