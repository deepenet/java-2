package Tests;

import Calculator.Calculator;
import Operation.Binary.DIV;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class DIVTest extends BinaryOperationTest {

    @Test
    public static void runTest() {
        executionContext = new Calculator.ExecutionContext();
        operation = new DIV();
        list = new ArrayList<>();
        /*
         *   0 / 1 = 0
         *   123 / 1 = 123
         *   369 / -123 = -3
         *   - 1e4 * 1e3 = -1e1
         */
        try{
            actualTest(0.0, 1.0, 0.0);
            actualTest(123.0, 1.0, 123.0);
            actualTest(369.0, -123.0, -3.0);
            actualTest(-1e4, 1e3, -1e1);
        } catch (Exception e) {
            Assert.fail("Unexpected exception " + e.getMessage());
        }

    }
}