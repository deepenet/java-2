package Tests;

import Calculator.Calculator;
import Operation.Binary.MULT;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class MULTTest extends BinaryOperationTest {
    @Test
    public static void runTest() {
        executionContext = new Calculator.ExecutionContext();
        operation = new MULT();
        list = new ArrayList<>();
        /*
         *   0 * 0 = 0
         *   0 * 123 = 0
         *   -123 * 0 = 0
         *   1 * 123 = 123
         *   -123 * -1 = 123
         *   - 1e3 * 1e4 = -1e7
         */
        try{
            actualTest(0.0, 0.0, 0.0);
            actualTest(0.0, 123.0, 0.0);
            actualTest(-123.0, 0.0, -0.0);
            actualTest(1.0, 123.0, 123.0);
            actualTest(-123.0, -1.0, 123.0);
            actualTest(-1e3, 1e4, -1e7);
        } catch (Exception e) {
            Assert.fail("Unexpected exception " + e.getMessage());
        }

    }
}