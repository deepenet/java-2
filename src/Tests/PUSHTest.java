package Tests;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Operation.*;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PUSHTest {

    @Test
    public static void runTest() {
        Calculator.ExecutionContext executionContext = new Calculator.ExecutionContext();
        Operation operation = new PUSH();
        List<String> list = new ArrayList<>();
        try{
            try{
                operation.perform(executionContext, list);
                Assert.fail("Expected BadOperationParametersException");
            } catch (BadOperationParametersException e) {
                assertNotEquals("", e.getMessage());
            }
            list.add("1");
            operation.perform(executionContext, list);
            list.add("1");
            try{
                operation.perform(executionContext, list);
                Assert.fail("Expected BadOperationParametersException");
            } catch (BadOperationParametersException e) {
                assertNotEquals("", e.getMessage());
            }
        } catch (Exception e) {
            Assert.fail("Unexpected exception " + e.getMessage());
        }

    }
}