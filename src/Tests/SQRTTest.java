package Tests;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Exceptions.StackEmptyException;
import Operation.Unary.SQRT;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class SQRTTest extends UnaryOperationTest {

    static private void actualTest(Double value, Double expected) throws StackEmptyException, BadOperationParametersException {
        executionContext.push(value);
        operation.perform(executionContext, list);
        assertEquals(expected, executionContext.showBack());
        executionContext.pop();
    }


    @Test
    public static void runTest() {
        executionContext = new Calculator.ExecutionContext();
        operation = new SQRT();
        list = new ArrayList<>();
        /*
         *   sqrt(0) = 0
         *   sqrt(1) = 1
         *   sqrt(4) = 2
         *   sqrt(196) = 14
         *   sqrt(2) = 1.4142135623730951
         */
        try{
            actualTest(0.0, 0.0);
            actualTest(1.0, 1.0);
            actualTest(4.0, 2.0);
            actualTest(196.0, 14.0);
            actualTest(2.0, 1.4142135623730951);
        } catch (Exception e) {
            Assert.fail("Unexpected exception " + e.getMessage());
        }

    }

}