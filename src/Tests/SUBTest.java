package Tests;

import Calculator.Calculator;
import Operation.Binary.SUB;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class SUBTest extends BinaryOperationTest {

    @Test
    public static void runTest() {
        executionContext = new Calculator.ExecutionContext();
        operation = new SUB();
        list = new ArrayList<>();
        /*
         *   0 - 0 = 0
         *   456 - 123 = 333
         *   4.56 + 1.23 = 5.79
         *   - 1e3 + 1e4 = 9.9e3
         */
        try{
            actualTest(0.0, 0.0, 0.0);
            actualTest(456.0, 123.0, 333.0);
            actualTest(4.0, -1.0, 5.0);
            actualTest(-1e3, -1e4, 9e3);
        } catch (Exception e) {
            Assert.fail("Unexpected exception " + e.getMessage());
        }

    }
}