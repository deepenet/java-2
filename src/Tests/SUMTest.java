package Tests;

import Calculator.Calculator;
import Operation.Binary.SUM;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class SUMTest extends BinaryOperationTest {

    @Test
    public static void runTest() {
        executionContext = new Calculator.ExecutionContext();
        operation = new SUM();
        list = new ArrayList<>();
        /*
        *   0 + 0 = 0
        *   123 + 456 = 579
        *   4.56 - 1.23 = 3.33
        *   1e3 - 1e4 = -9.9e3
        */
        try{
            actualTest(0.0, 0.0, 0.0);
            actualTest(123.0, 456.0, 579.0);
            actualTest(4.0, -1.0, 3.0);
            actualTest(1e3, -1e4, -9e3);
        } catch (Exception e) {
            Assert.fail("Unexpected exception " + e.getMessage());
        }

    }
}