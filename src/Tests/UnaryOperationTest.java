package Tests;

import Calculator.Calculator;
import Exceptions.BadOperationParametersException;
import Exceptions.StackEmptyException;
import Operation.Unary.SQRT;
import Operation.Unary.UnaryOperation;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UnaryOperationTest {
    protected static Calculator.ExecutionContext executionContext;
    protected static UnaryOperation operation;
    protected static List<String> list;

    @Test
    public static void runTest() {
        executionContext = new Calculator.ExecutionContext();
        list = new ArrayList<>();
        operation = new SQRT();
        try {
            try {
                operation.perform(executionContext, list);
            } catch (StackEmptyException e) {
                assertNotEquals("", e.getMessage());
            }
            executionContext.push(1.1);
            operation.perform(executionContext, list);
            list.add("First_needless");
            try {
                operation.perform(executionContext, list);
            } catch (BadOperationParametersException e) {
                assertNotEquals("", e.getMessage());
            }
            list.add("Second_needless");
            try {
                operation.perform(executionContext, list);
            } catch (BadOperationParametersException e) {
                assertNotEquals("", e.getMessage());
            }
        } catch (Exception e) {
            Assert.fail("Unexpected exception " + e.getMessage());
        }
    }
}